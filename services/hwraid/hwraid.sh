#!/bin/bash

##################################################
# Service - exemple
#
# This is an exemple of service script file
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared functions
####################


####################
# Install
####################

PREINSTALL()
{
    :
}

POSTINSTALL()
{
    wget -O - https://hwraid.le-vert.net/debian/hwraid.le-vert.net.gpg.key | apt-key add -

    apt-get update
    apt-get install -y megacli

    # Disable the repository
    sed -i "s@^deb http://hwraid.le-vert.net/debian stretch main@#deb http://hwraid.le-vert.net/debian stretch main@g" /etc/apt/sources.list.d/hwraid.list
    apt-get update
}


####################
# Update
####################

PREUPDATE()
{
    :
}

POSTUPDATE()
{
    apt-get update
    apt-get upgrade

    # Disable the repository
    sed -i "s@^deb http://hwraid.le-vert.net/debian stretch main@#deb http://hwraid.le-vert.net/debian stretch main@g" /etc/apt/sources.list.d/hwraid.list
    apt-get update
}


####################
# Backup / Restore
####################

BACKUP()
{
    # Get backup path from main.sh
    backup_path=$1

    # Remove this line when implemented
    echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
    # Get restore path from main.sh
    restore_path=$1

    # Remove this line when implemented
    echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
    POSTINSTALL
    ;;
    PREUPDATE)
    PREUPDATE
    ;;
    POSTUPDATE)
    POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
    echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
