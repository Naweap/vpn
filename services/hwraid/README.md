Descriptif du service
=====================

Surveillance disque - Controleur RAID pour par example les serveur DELL.

Info particulière déployement
-----------------------------

A installer sur les hyperviseurs.

Documentation : https://wikitech.wikimedia.org/wiki/MegaCli

Dépot apt : https://hwraid.le-vert.net/wiki/DebianPackages

Cas de test après dépoyement
----------------------------

- Tester que la commande `megacli -PDList -aall` retourne bien l'état des disques

TODO
----

Voir Fichier CONTRIBUTING
