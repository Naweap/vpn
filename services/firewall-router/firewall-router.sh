#!/bin/bash

##################################################
# Service - firewall
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared variables
####################

dependances="python3-yaml python3-psycopg2"

####################
# Shared functions
####################


####################
# Install
####################

PREINSTALL()
{
	apt-get -y install $dependances
	mkdir -p /var/cache/vpn_routes
	touch /var/cache/vpn_routes/ip4_routes.sh
	touch /var/cache/vpn_routes/ip6_routes.sh
}

POSTINSTALL()
{
    chmod 700 /etc/init.d/iptables
    chmod 700 /etc/init.d/ip6tables
    chmod u+x /usr/local/bin/coin-sync
    systemctl enable iptables
    systemctl enable ip6tables
    /etc/init.d/iptables restart
    /etc/init.d/ip6tables restart
}


####################
# Update
####################

PREUPDATE()
{
	apt-get -y install $dependances
}

POSTUPDATE()
{
    chmod 700 /etc/init.d/iptables
    chmod 700 /etc/init.d/ip6tables
    chmod u+x /usr/local/bin/coin-sync
    systemctl daemon-reload
    /etc/init.d/iptables restart
    /etc/init.d/ip6tables restart
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Nothing to do  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Nothing to do $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
