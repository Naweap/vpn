#!/usr/bin/perl -w

use strict;
use warnings;

############################################################################################

# A adapter selon la configuration

my $zone_file = $ARGV[0];

############################################################################################

my $fh;

$ENV{PATH} = '/usr/sbin:/usr/bin:/sbin:/bin';

# # Lecture de l'encien fichier

my $file = "";

open ($fh, '<', $zone_file) or die "Erreur : impossible d'ouvrir le fichier '$zone_file'";

while (<$fh>)
{
	my $ligne = "$_";
	if (/(.*)\d(\s*; Serial)/) {
	  
		my ($valeur) = split(/;/,$_);
		$valeur = 1 + "$valeur";
		
		if (s/(\s*)\d*(\s*; Serial)/$1$valeur$2/) {
		
		 	$file = "$file$_"; 
		}
	}
	else 
	{
		$file = "$file$ligne";
	}
}
close $fh;

# Mise a jours du fichier de Zone
open ($fh, '>', $zone_file) or die "Erreur : impossible d'ouvrir le fichier '$zone_file'";

print $fh "$file";

close $fh;

print "\n";
