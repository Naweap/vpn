$ORIGIN swissneutral.ch.
$TTL 3600
@   IN   SOA    __DNS_AUTH_MASTER_FQDN__.     __ADMIN_EMAIL_DNS__. (
                __SERIAL_ZONE_NUMBER__01      ; Serial
                3600            ; Refresh
                1800            ; Retry
                1209600         ; Expire
                43200 )         ; Negative Cache TTL

$INCLUDE "../keys/Kswissneutral.ch.zsk.key" ;
$INCLUDE "../keys/Kswissneutral.ch.ksk.key" ;

                        IN  NS      __DNS_AUTH_MASTER_FQDN__.
                        IN  NS      __DNS_AUTH_SLAVE_1_FQDN__.
                        IN  NS      ns6.gandi.net.

@                       IN  A       91.121.14.63
@                       IN  AAAA    2001:41d0:1:613f::

ns1                     IN  A       __DNS_AUTH_MASTER_IP4__
ns1                     IN  AAAA    __DNS_AUTH_MASTER_IP6__
ns2                     IN  A       __DNS_AUTH_SLAVE_1_IP4__
ns2                     IN  AAAA    __DNS_AUTH_SLAVE_1_IP6__


;_443._tcp.swissneutral.ch.     IN TLSA 3 0 1 TLSA_VALEUR_A_METTRE

;_25._tcp.swissneutral.ch.      IN TLSA 3 0 1 TLSA_VALEUR_A_METTRE
;_465._tcp.swissneutral.ch.     IN TLSA 3 0 1 TLSA_VALEUR_A_METTRE
;_993._tcp.swissneutral.ch.     IN TLSA 3 0 1 TLSA_VALEUR_A_METTRE

swissneutral.ch.           CAA 0   issue "letsencrypt.org"
swissneutral.ch.           CAA 0   iodef "mailto:__ADMIN_EMAIL_DNS__"
