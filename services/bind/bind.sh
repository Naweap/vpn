#!/bin/bash

##################################################
# Service - bind
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared variables
####################

dependances="bind9 dnsutils haveged bc python3-yaml python3-psycopg2"

####################
# Shared functions
####################

workaround_domain_for_test_infra() {
    if [ $MAIN_DOMAIN == "test.swissneutral.net" ]; then
        mv /etc/bind/zones/db.swissneutral.net /etc/bind/zones/db.test.swissneutral.net
    fi
}

####################
# Install
####################

PREINSTALL()
{
	# Installation de Bind9
	apt-get -y install $dependances
	mkdir -p /var/local/lib/coin-sync/
	echo "0" > /var/local/lib/coin-sync/update_counter
}


POSTINSTALL()
{
    chmod 700 /usr/local/bin/dns-manager
    chmod 700 /usr/local/bin/update_zone_serial.perl
    chmod u+x /usr/local/bin/coin-sync
    systemctl restart bind9
    systemctl restart cron
    coin-sync || true # Because it might be not already configured
    workaround_domain_for_test_infra
    dns-manager update_all
}


####################
# Update
####################

PREUPDATE()
{
	apt-get -y install $dependances
	mkdir -p /var/local/lib/coin-sync/
	echo "0" > /var/local/lib/coin-sync/update_counter
}


POSTUPDATE()
{
    chmod 700 /usr/local/bin/dns-manager
    chmod 700 /usr/local/bin/update_zone_serial.perl
    chmod u+x /usr/local/bin/coin-sync
    systemctl restart bind9
    systemctl restart cron
    coin-sync
    workaround_domain_for_test_infra
    dns-manager update_zone
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
