Descriptif du service
=====================

Service déployant yunohost ainsi que un mécanisme de mise a jours de sécurité automatique et par email pour toutes autre mise à jours.

Info particulière déployement
-----------------------------

!! Attention ce service actuellement peut être déployé uniquement dans un container LXC en UNPRIVILEGIED.

Lord de l'installation du service le script doit être exécuté directement sur la VM (via SSH) et non via script qui crée automatiquement la connexion SSH.

Ce service implémente de manière interne déjà un firewall et une gestion des mises à jours système + yunohost.

La postinstallation est cassée avec LXC 3.0 voir PR : https://github.com/YunoHost/yunohost/pull/508

Il faut en plus pour que MariaDB démarre activer l'option "nexting=1"

Cas de test après dépoyement
----------------------------

- Tester le fonctionnement du serveur WEB
- Tester le fonctionnement du serveur Mail
- Tester le fonctionnement du Firewall

TODO
----

Voir Fichier CONTRIBUTING
