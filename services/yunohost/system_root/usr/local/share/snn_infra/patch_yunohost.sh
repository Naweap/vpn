#!/bin/bash

patch_count=$(grep -c '# SNN PATCH FOR COIN FULL ACCESS IN LDAP DATABASE' /usr/share/yunohost/templates/slapd/slapd.ldif)

if [ $patch_count -ne 3 ]
then
    echo "Try to patch LDAP config"
    # If the patch was applied, it was needed and we need to apply the new time the slapd config by the yunohost regen-conf
    patch -f /usr/share/yunohost/templates/slapd/slapd.ldif < /usr/local/share/snn_infra/yunohost_patch_LDAP_coin_access.patch && \
        yunohost tools regen-conf slapd && systemctl restart slapd && systemctl restart coin
fi

patch_count_access_lua=$(grep -c '\--- SNN PATCH - BLOCK USER EDITION IN SSOWAT' /usr/share/ssowat/access.lua)
patch_count_portal_css=$(grep -c '/* SNN PATCH - BLOCK USER EDITION IN SSOWAT' /usr/share/ssowat/portal/assets/css/ynh_portal.css)
patch_count_portal_html=$(grep -c '<!--   SNN PATCH - BLOCK USER EDITION IN SSOWAT -->' /usr/share/ssowat/portal/portal.html)

if [ $patch_count_access_lua -ne 1 ] || [ $patch_count_portal_css -ne 1 ] || [ $patch_count_portal_html -ne 1 ]
then
    echo "Try to patch SSOwat"
    pushd /usr/share/ssowat/
    patch -f -p1 < /usr/local/share/snn_infra/yunohost_ssowat_email_edit.patch && systemctl restart nginx
    popd
fi
