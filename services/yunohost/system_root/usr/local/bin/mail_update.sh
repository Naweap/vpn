#!/bin/bash

retourn_update=$(yunohost tools update)

if [[ "$retourn_update" !=  *"Il n'y a rien à faire ! Tout est déjà à jour !"* ]]
then
	if [[ "$(cat /tmp/send_mail_update.txt)" != "$retourn_update" ]]
	then
		echo "$retourn_update" > /tmp/send_mail_update.txt
		chmod 600 /tmp/send_mail_update.txt
		echo "$retourn_update" | mail -a "Content-Type: text/plain; charset=UTF-8" -s "Nouvelles Mises à jours Yunohost" __ADMIN_EMAIL__
	fi
fi

exit 0
