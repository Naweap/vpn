#!/bin/bash

##################################################
# Service yunohost
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared functions
####################


####################
# Install
####################

PREINSTALL()
{
	echo "This service need to be installed localy not by script witch use a remote command throught ssh"
	echo "This service need an unprivilegied container !!"

	apt-get -y install git dialog ca-certificates systemd-sysv
}

POSTINSTALL()
{
	git clone https://github.com/YunoHost/install_script /tmp/install_script
	old_pwd=$PWD
	cd /tmp/install_script
	
	echo "An interactive install will be lauched, you need to accept everything except the postinstall : You need to reboot the container before to do the postinstall"
	sleep 10
	./install_yunohost
	cd $old_pwd
	
	echo "!! You need to reboot the container before to do the postinstall !!"
	
# 	yunohost postinstall -d "$YUNOHOST_DOMAIN" -p "$ADMIN_PASSWORD"  --ignore-dyndns
	
    mkdir -p /home/admin/.ssh
    cp /root/.ssh/authorized_keys /home/admin/.ssh/authorized_keys
    chmod 700 /home/admin/.ssh
    chmod 600 /home/admin/.ssh/authorized_keys
    chmod 700 /usr/local/bin/mail_update.sh
    chmod 700 /usr/local/share/snn_infra/patch_yunohost.sh
    systemctl daemon-reload
}


####################
# Update
####################

PREUPDATE()
{
    rm -f /usr/local/bin/patch_yunohost.sh
}

POSTUPDATE()
{
    chmod 700 /usr/local/bin/mail_update.sh
    chmod 700 /usr/local/share/snn_infra/patch_yunohost.sh
    systemctl daemon-reload 
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	yunohost backup create -o "$backup_path"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	yunohost backup restore "$restore_path"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
