Descriptif du service
=====================

Configure la zone de temps et le serveur NTP.



Cas de test après dépoyement
----------------------------

La commande "timedatectl" doit retourner la date, l'heure et la zone de temps exacte.


La commande "systemctl status systemd-timesyncd | grep server" doit afficher une synchronisation avec succès au serveur de temps. (ntp.metas.ch)

