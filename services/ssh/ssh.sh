#!/bin/bash

##################################################
# Service - ssh
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared functions
####################

admins_keys_location="admin_keys/"
start_admin_key_bloc="######### BEGIN AUTOMATIC ADMIN KEYS #########"
stop_admin_key_bloc="######### END AUTOMATIC ADMIN KEYS #########"

####################
# Admins public keys
####################

add_admins_keys()
{
    tmp_file=$(mktemp)
    
    admin_keys=""
    for admin_name in $ADMINS_LIST
    do
        admin_key="$(cat $data_folder/$admins_keys_location$admin_name.pub \
            | sed -z 's|\\|\\\\|g' \
            | sed -z 's@|@\\|@g' \
            | sed -z 's|\n|\\n|g')"
        admin_keys+="\n$admin_key"
    done

    if grep -q "$start_admin_key_bloc" $SSH_AUTHORIZED_KEYS_LOCTION; then
        sed -i -z "s|$start_admin_key_bloc.*$stop_admin_key_bloc|$start_admin_key_bloc$admin_keys$stop_admin_key_bloc|g" $SSH_AUTHORIZED_KEYS_LOCTION
    else
        echo "$start_admin_key_bloc" >> $SSH_AUTHORIZED_KEYS_LOCTION
        echo -e "$admin_keys" >> $SSH_AUTHORIZED_KEYS_LOCTION
        echo "$stop_admin_key_bloc" >> $SSH_AUTHORIZED_KEYS_LOCTION
    fi
}


####################
# Install
####################

PREINSTALL()
{
	apt-get install -y openssh-server
}

POSTINSTALL()
{
	# Generate rsa keys if it not exist
	test -e /root/.ssh/id_ed25519 || ssh-keygen -t ed25519 -f /root/.ssh/id_ed25519 -q -N ""
	test -e /root/.ssh/id_rsa || ssh-keygen -b $SSH_KEY_LENGTH -t rsa -f /root/.ssh/id_rsa -q -N ""

	add_admins_keys

	# Restart service
	service ssh restart
}


####################
# Update
####################

PREUPDATE()
{
	:
}

POSTUPDATE()
{
	# Add updated admins keys
	add_admins_keys

	# Restart service
	service ssh restart
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
