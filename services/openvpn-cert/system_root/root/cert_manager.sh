#!/bin/bash

Openvpn_CA_Home="/root/openvpnCA"
Openvpn_CA_Final_Dest="/root/openvpn_Server_Cert"
Openvpn_CA_Client_Final_Dest="/root/openvpn_Client_Cert"


fist_lauch() {
    echo "DO YOU REALLY WANT TO REMOVE ALL OLD KEY AND CA ? [yes/no]"
    read answer
    if [[ $answer != "yes" ]]
    then
        echo "exit"
        exit 0
    fi
    echo "Chose between client or server mode [client/server]"
    read mode

    echo "Cleanup all data"
    rm -r "$Openvpn_CA_Home"
    rm -r "$Openvpn_CA_Final_Dest"
    cp -r /opt/EasyRSA-__EASYRSA_VERSION__ "$Openvpn_CA_Home"
    cp -r /opt/easyRSA_swissneutral_config_${mode}/openssl.cnf "$Openvpn_CA_Home"
    cp -r /opt/easyRSA_swissneutral_config_${mode}/vars "$Openvpn_CA_Home"
    mkdir "$Openvpn_CA_Final_Dest"

    rm -r "$Openvpn_CA_Home_Client"
    mkdir "$Openvpn_CA_Client_Final_Dest"

    create_ca
    [[ $mode == "server" ]] && create_server_cert
}

create_ca() {
    # CA for server
    cd "$Openvpn_CA_Home"

    ./easyrsa init-pki
    ./easyrsa build-ca
}

create_server_cert() {
    cd "$Openvpn_CA_Home"

    echo "Création des clef serveur"
    ./easyrsa build-server-full server nopass

    ./easyrsa gen-dh
    openvpn --genkey --secret pki/ta.key

    cp "$Openvpn_CA_Home/pki/ca.crt" "$Openvpn_CA_Final_Dest"
    cp "$Openvpn_CA_Home/pki/issued/server.crt" "$Openvpn_CA_Final_Dest"
    cp "$Openvpn_CA_Home/pki/private/server.key" "$Openvpn_CA_Final_Dest"
    cp "$Openvpn_CA_Home/pki/dh.pem" "$Openvpn_CA_Final_Dest"
    cp "$Openvpn_CA_Home/pki/ta.key" "$Openvpn_CA_Final_Dest"
    echo "All keys are available in $Openvpn_CA_Final_Dest"
}

create_client_cert() {
    cd "$Openvpn_CA_Home"
    
    echo "Cration d'un certificat client"
    echo "Donnez un nom pour ce client (sans espace ou caractères spéciaux)"
    read name
    
    if [[ -e "$Openvpn_CA_Home/pki/issued/$name.crt" ]]
    then
        echo "Ce nom existe déjà voulez vous l'écraser ? [yes/no]"
        read answer

        while [[ $answer != "yes" ]] && [[ -e "$Openvpn_CA_Home/pki/issued/$name.crt" ]]
        do
            echo "Ce nom existe déjà donnez un autre nom pour ce client (sans espace ou caractères spéciaux)"
            read name
        done
    fi

    ./easyrsa build-client-full "$name" nopass

    cp "$Openvpn_CA_Home/pki/issued/$name.crt" "$Openvpn_CA_Client_Final_Dest"
    cp "$Openvpn_CA_Home/pki/private/$name.key" "$Openvpn_CA_Client_Final_Dest"
    echo "The client keys and certificate is available in $Openvpn_CA_Client_Final_Dest"
}

sign-extern-client-cert() {
    cd "$Openvpn_CA_Home"
    
    echo "Signature d'un certificat externe"
    echo "Donner un chemin complet vers le fichier de requête de signature"
    read request_path
    if [[ ! -e "$request_path" ]]
    then
        echo "Erreur : ce fichier n'existe pas"
        exit 1
    fi

    echo "Donnez un nom pour ce client (sans espace ou caractères spéciaux)"
    read name
    
    if [[ -e "$Openvpn_CA_Home/pki/issued/$name.crt" ]]
    then
        echo "Ce nom existe déjà voulez vous l'écraser ? [yes/no]"
        read answer

        while [[ $answer != "yes" ]] && [[ -e "$Openvpn_CA_Home/pki/issued/$name.crt" ]]
        do
            echo "Ce nom existe déjà donnez un autre nom pour ce client (sans espace ou caractères spéciaux)"
            read name
        done
    fi

    ./easyrsa import-req $request_path $name
    ./easyrsa sign-req client $name
    cp "$Openvpn_CA_Home/pki/issued/$name.crt" "$Openvpn_CA_Client_Final_Dest"
    echo "The client certificate is available in $Openvpn_CA_Client_Final_Dest"
}

case $1 in
    first-launch)
        fist_lauch
    ;;
    create-ca)
        create_ca
    ;;
    create-client-cert)
        create_client_cert
    ;;
    create-server-cert)
        create_server_cert
    ;;
    sign-extern-client-cert)
        sign-extern-client-cert
    ;;
    *)
    echo "Functions :   first-launch        : Create a CA and a certificate and a dh for the server"
    echo "              create-ca           : Juste create a new CA or renew it, in this case it keep all old data"
    echo "              create-client-cert  : Create a certificate for a client"
    echo "              sign-extern-client-cert : Sign a certificate from a client"
esac
