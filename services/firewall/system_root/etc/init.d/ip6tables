#!/bin/bash

### BEGIN INIT INFO
# Provides:          Ip6tables
# Required-Start:    $network $named
# Required-Stop:     $network 
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Firefall IPv6 specific VM - LXC
# Description:       Firefall IPv6 specific VM - LXC
### END INIT INFO

DEAMON_NAME="ip6tables"
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
IPTABLES="ip6tables -w 10"

FIREWALL_SSH_PORT=__SSH_PORT__
FIREWALL_UDP_PORT="__FIREWALL_UDP_PORT__"
FIREWALL_TCP_PORT="__FIREWALL_TCP_PORT__"

start() {
    # On netoie les tables avant d'ajouter des règles
    $IPTABLES -t filter -F
    $IPTABLES -t filter -X
    $IPTABLES -t nat -F
    $IPTABLES -t nat -X
    $IPTABLES -t mangle -F
    $IPTABLES -t mangle -X

    # Regles par defauts
    $IPTABLES -P INPUT DROP
    $IPTABLES -P FORWARD DROP
    $IPTABLES -P OUTPUT ACCEPT
    $IPTABLES -A INPUT -p icmpv6 -j ACCEPT
    $IPTABLES -A FORWARD -p icmpv6 -j ACCEPT

    # Regles INPUT globales
    $IPTABLES -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
    $IPTABLES -A INPUT -i lo -j ACCEPT

    for p in $FIREWALL_UDP_PORT
    do
        $IPTABLES -A INPUT -p udp --dport $p -j ACCEPT
    done

    for p in $FIREWALL_TCP_PORT
    do
        $IPTABLES -A INPUT -p tcp --dport $p -j ACCEPT
    done
    
    __FIREWALL_IP6_CUSTOM_RULE__

    # Règles spécifiques
    $IPTABLES -A INPUT -p tcp --dport $FIREWALL_SSH_PORT -j ACCEPT
}

stop() {
    $IPTABLES -P INPUT ACCEPT
    $IPTABLES -P FORWARD ACCEPT
    $IPTABLES -P OUTPUT ACCEPT

    #init des tables NAT et MANGLE
    $IPTABLES -t nat -F
    $IPTABLES -t nat -X
    
    $IPTABLES -t mangle -F
    $IPTABLES -t mangle -X
}

case "$1" in
    start)
        start
    ;;
    restart|reload|force-reload)
        stop
        start
    ;;
    stop)
        stop
    ;;
    status)
        echo "not implemented"
        $IPTABLES -L --verbose -n
        $IPTABLES -t nat -L --verbose -n
    ;;
    *)
    echo "Usage: /etc/init.d/$DEAMON_NAME {start|stop|force|restart|reload|force-reload|status}"
    exit 1
    ;;
esac
exit 0
