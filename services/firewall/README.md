Descriptif du service
=====================

Service permettant de configurer des règles de base et avancée pour une VM.


Info particulière déployement
-----------------------------

Afin de pouvoir ajouter des règles de firewall particulières il est possible ajouter simplement toutes les règles de la manière suivante par example :

```bash
FIREWALL_IP4_CUSTOM_RULE="
    iptables -A FORWARD -i $WAN_INTERFACE -o tun0 -j ACCEPT
    iptables -A FORWARD -i tun0 -o $WAN_INTERFACE -j ACCEPT
"
```

Il est aussi naturellement possible de faire de même pour IPv6 avec `FIREWALL_IP6_CUSTOM_RULE`.


Cas de test après dépoyement
----------------------------

- Vérifier que la règle par défaut est bien `DROP` pour les chaine FORWARD et INPUT.
- Vérifier que tous les port qui doivent être ouvert sont bien ouverts.

TODO
----

Voir Fichier CONTRIBUTING
