#!/usr/bin/env bash

##################################################
# Service - unbound
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared functions
####################

# Variables
unbound_files="/etc/unbound"


####################
# Install
####################

PREINSTALL()
{
    apt-get install -y unbound dnsutils unbound-host dnssec-trigger dns-root-data
    
    # Génération des clefs pour DNS over TLS
    openssl req -x509 -newkey rsa:4096 \
        -keyout /etc/unbound/privatekeyfile.key -out /etc/unbound/publiccertfile.pem \
        -days 1000 -nodes

    mkdir -p /var/log/unbound
    chown unbound -R /var/log/unbound
}

POSTINSTALL()
{
   service unbound restart
}


####################
# Update
####################

PREUPDATE()
{
    :
}


POSTUPDATE()
{
    :
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	cp -r /var/log/unbound $backup_path
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
