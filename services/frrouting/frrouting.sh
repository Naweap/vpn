#!/bin/bash

##################################################
# Service - quagga
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared functions
####################


####################
# Install
####################

PREINSTALL()
{
    apt-get install -y frr

    # Fix pour LXC
    sed -i "s/^Nice=-5/^#Nice=-5/g" /lib/systemd/system/frr.service
}

POSTINSTALL()
{
    mkdir -p /var/log/frr
    chown -R frr /var/log/frr
	systemctl restart frr || systemctl restart frr
}


####################
# Update
####################

PREUPDATE()
{
    # Fix pour LXC
    sed -i "s/^Nice=-5/^#Nice=-5/g" /lib/systemd/system/frr.service

    apt-get install -y frr
}

POSTUPDATE()
{
	systemctl restart frr || systemctl restart frr
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
