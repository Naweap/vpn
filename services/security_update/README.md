Descriptif du service
=====================

Service s'occupant de mettre à jours automatiquement les mises à jours de sécurité. Les autres mises à jours générerons un email qui sera envoyé aux administrateurs.

Info particulière déployement
-----------------------------




Cas de test après dépoyement
----------------------------

- Tester les mises à jours de sécurité qu'elle se fassent automatiquement
- Tester que les mises à jours (pas de sécurité) génère un email envoyé a tous les admin.

TODO
----

Voir Fichier CONTRIBUTING
