Descriptif du service
=====================

Ce service permettra de mettre en place un terminal virtuel sur toutes les machines. Il permettra aussi de logger toutes les actions effectuées par l'administrateur.

Info particulière déployement
-----------------------------




Cas de test après dépoyement
----------------------------

- Se connecter et vérifier qu'un message indique à l'administrateur d'utiliser tmux pour toutes les opérations.
- Ouvrir tmux

TODO
----

Voir Fichier CONTRIBUTING
