

tmux-export-session() {
    # Manage timux
    tmux pipe-pane
    local d="$(date +%Y-%m-%d_%H.%M)"
    tmux pipe-pane -o "cat >> /root/tmux/${d}_${1}.log"

    # Overrride nano command
    nano() {
        local file_path="$(echo "$@" | cut -d' ' -f$#)"
        local d="$(date +%Y-%m-%d_%H.%M.%S)"
        
        if [[ "$file_path" =~ ^/.* ]]; then
            # Full path
            skipped_file_path="${file_path//\//_}"
        else
            # Relative path
            local _pwd="$(pwd)"
            skipped_file_path="${_pwd//\//_}_${file_path//\//_}"
        fi

        test -e $file_path && \
            cp $file_path "/root/tmux/nano/${d}_pre-edit${skipped_file_path}"
        /bin/nano $@
        test -e $file_path && \
            cp $file_path "/root/tmux/nano/${d}_post-edit${skipped_file_path}"
    }
}
