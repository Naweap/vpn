Descriptif du service
=====================

Serveur DNS authoritaire slave pour les zones de l'association. Le serveurs utilisé sera bind9. 


Cas de test après dépoyement
----------------------------

- Tester la résolution DNS pour les domaine de l'association.
    - Tester tous les types d'enregistrement DNS (A, AAAA, PTR, MX, TXT (srv), DNSSEC)
- Tester DNSSEC (quand ca sera déployé).
- Tester la sychronisation entre serveur master et slave.

TODO
----

Voir Fichier CONTRIBUTING
