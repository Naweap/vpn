Descriptif du service
=====================

Configure correctement les locales sur les VM

Info déployement
----------------

Généralement on choisira les paramètres locaux fr_CH.UTF-8 donc soit la valeur "224" lord de la configuration.

Cas de test après dépoyement
----------------------------

Via la commande `locale` on peut vérifier que les locales sont configurée correctement et que toutes les variables sont définies.

TODO
----

Voir Fichier CONTRIBUTING
