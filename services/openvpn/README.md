Descriptif du service
=====================

Service dépoyant le serveur OpenVPN ainsi que la connexion au serveur LDAP pour l'authentification des USERS.

Info particulière déployement
-----------------------------

### Interface tunnel dans les container LXC

Afin que le serveur OpenVPN puisse créer des interface de tunneling il est nécessaire d'ajouter la ligne suivante dans le fichier `/etc/pve/lxc/no_conainter.conf`

```
lxc.mount.entry: /dev/net dev/net none bind,create=dir
```

Il faut en plus pour que OpenVPN démarre activer l'option "nexting=1"

### Accès aux donnée de coin

Après installation du service il est nécessaire de configurer les identifiant d'accès à la base de donnée coin.

Pour se connecter à postgresql utiliser la commande : `sudo -u postgres psql -d coin`:

Commandes création de l'utilisateurs avec les permissions nécessaires:
```
CREATE USER openvpn_access_info WITH
    LOGIN
    NOSUPERUSER
    NOCREATEDB
    NOCREATEROLE
    NOINHERIT
    NOREPLICATION
    CONNECTION LIMIT -1
    PASSWORD 'THE BEST PASSWORD';
GRANT CONNECT ON DATABASE coin TO openvpn_access_info;
GRANT SELECT ON TABLE public.vpn_vpnconfiguration,
                      public.configuration_configuration,
                      public.resources_ipsubnet,
                      public.offers_offersubscription,
                      public.members_member
TO openvpn_access_info;
```

Il est ensuite nécessaire d'ajouter la ligne suivante dans le fichier `/etc/postgresql/11/main/pg_hba.conf` :
```
host coin openvpn_access_info fd0a:db40:0:1::2/128 password
```

D'autre part Postgresql doit écouter sur le LAN IPv6 nécessaire. Editer ainsi la ligne du fichier `/etc/postgresql/11/main/postgresql.conf` :
```
port = 5432 # Parfois c'est mis à 5433
listen_addresses = 'localhost, fd0a:db40:0:1::4'
```

Créer un fichier dans `/etc/coin-sync-access.yml` et lui mettre les donnée suivantes :
```yml
db_address :  "1.2.3.4"
db_name :     "coin"
db_username : "openvpn_access_info"
db_password : "THE BEST PASSWORD"
```

Ensuite il faut mettre des permission restrictives sur ce fichier (chmod, chown) !!

Cas de test après déploiement
----------------------------

- Tester connexion au serveur OpenVPN via UDP sur le port 1194 et via TCP sur le port 443.
- Tester l'attribution d'adresses IP.
- Tester la configuration des routes vers les clients.
- Tester le routage des subnets IPv4 et IPv6 vers les clients.
- Tester la vérification des certification utilisateurs en fonction des fichiers de configuration.
- Tester la synchronisation de la configuration avec coin :
    - Tester que les configurations VPN actives dans coin génère un fichier de configuration par client OpenVPN.
    - Tester que le numéro de série du certificat délivré sur coin permette de se connecter au serveur OpenVPN.
    - Tester qu'il s'agit uniquement du dernier certificat délivré qui permet de se connecter au VPN.
    - Tester que un numéro de série pas encore définit (dans coin) ne génère pas de bug. Le client doit simplement pas pouvoir se connecter au VPN.
    - Tester que tous les subnets définit dans coin soient routé vers le client.

TODO
----

Voir Fichier CONTRIBUTING
