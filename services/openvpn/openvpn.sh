#!/bin/bash

##################################################
# Service - openvpn
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared variables
####################

dependances="openvpn openvpn-auth-ldap sudo socat python3-yaml python3-psycopg2"

####################
# Shared functions
####################

set_permission() {
	chown openvpn: /var/log/openvpn
	chown openvpn: -R /etc/openvpn
	chmod u=rwX,g=rX,o= /var/log/openvpn
	chmod u=rwX,g=rX,o= /etc/openvpn
	chmod u+x /etc/openvpn/handler
	chmod u+x /usr/local/bin/coin-sync
}

enable_service() {
    systemctl enable openvpn-server@vpn_conf_1
    systemctl enable openvpn-server@vpn_conf_2
    systemctl restart openvpn-server@vpn_conf_1
    systemctl restart openvpn-server@vpn_conf_2
}

####################
# Install
####################
PREINSTALL()
{
	apt-get install -y $dependances
	mkdir -p $OPENVPN_SERVER1_CERT_FOLDER
    mkdir -p /etc/openvpn/ccd
}

POSTINSTALL()
{
	useradd openvpn
	mkdir -p /var/log/openvpn

	set_permission

    # configure SUDO
    chmod 400 /etc/sudoers.d/openvpn

	systemctl daemon-reload
	enable_service
	systemctl restart cron
}


####################
# Update
####################

PREUPDATE()
{
	apt-get install -y $dependances
}

POSTUPDATE()
{
	echo " " >> /etc/openvpn/auth/ldap.conf # Hack pour résoudre l'erreur "segfault"
	
	set_permission
	
    systemctl daemon-reload
	enable_service
	systemctl restart cron
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
