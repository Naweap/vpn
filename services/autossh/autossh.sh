#!/bin/bash

##################################################
# Service - autossh
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared functions
####################


####################
# Install
####################

PREINSTALL()
{
    apt-get install -y autossh

    useradd -s /bin/bash autossh
    mkdir -p /home/autossh/.ssh
    ssh-keygen -t ed25519 -f $SSH_PRIVATE_KEY_LOCTION -q -N ""
    chown autossh -R /home/autossh
}

POSTINSTALL()
{
    for server in $AUTOSSH_TARGET_IP4
    do
        serverName=${server##*'|'}
        user=${server%'@'*}
        ip_dest=${server#*'@'}
        ip_dest=${ip_dest%':'*}
        port=${server#*':'}
        port=${port%%'|'*}
        autossh_relay_listing_port=$(echo $server | cut -d'|' -f2)

        cp $data_folder/autossh.service /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__USER_DEST__/$user/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__IP_DEST__/$ip_dest/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__PORT_DEST__/$port/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__SERVER_TARGET_NAME__/$serverName/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__AUTOSSH_RELAY_LISTENING_PORT__/$autossh_relay_listing_port/g" /etc/systemd/system/autossh_$serverName.service

        systemctl enable autossh_$serverName.service
        systemctl start autossh_$serverName.service
    done

    for server in $AUTOSSH_TARGET_IP6
    do
        serverName=${server##*'|'}
        user=${server%'@'*}
        ip_dest=${server#*'['}
        ip_dest=${ip_dest%']'*}
        port=${server##*':'}
        port=${port%%'|'*}
        autossh_relay_listing_port=$(echo $server | cut -d'|' -f2)

        cp $data_folder/autossh.service /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__USER_DEST__/$user/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__IP_DEST__/$ip_dest/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__PORT_DEST__/$port/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__SERVER_TARGET_NAME__/$serverName/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__AUTOSSH_RELAY_LISTENING_PORT__/$autossh_relay_listing_port/g" /etc/systemd/system/autossh_$serverName.service

        systemctl enable autossh_$serverName.service
        systemctl start autossh_$serverName.service
    done
}

####################
# Update
####################

PREUPDATE()
{
    :
}

POSTUPDATE()
{
    for server in $AUTOSSH_TARGET_IP4
    do
        serverName=${server#*'|'}
        user=${server%'@'*}
        ip_dest=${server#*'@'}
        ip_dest=${ip_dest%':'*}
        port=${server#*':'}
        port=${port%'|'*}

        cp $data_folder/autossh.service /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__USER_DEST__/$user/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__IP_DEST__/$ip_dest/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__PORT_DEST__/$port/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__SERVER_TARGET_NAME__/$serverName/g" /etc/systemd/system/autossh_$serverName.service

        systemctl daemon-reload
        systemctl start autossh_$serverName.service
    done

    for server in $AUTOSSH_TARGET_IP6
    do
        serverName=${server#*'|'}
        user=${server%'@'*}
        ip_dest=${server#*'['}
        ip_dest=${ip_dest%']'*}
        port=${server##*':'}
        port=${port%'|'*}

        cp $data_folder/autossh.service /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__USER_DEST__/$user/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__IP_DEST__/$ip_dest/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__PORT_DEST__/$port/g" /etc/systemd/system/autossh_$serverName.service
        sed -i "s/__SERVER_TARGET_NAME__/$serverName/g" /etc/systemd/system/autossh_$serverName.service

        systemctl daemon-reload
        systemctl start autossh_$serverName.service
    done
}


####################
# Backup / Restore
####################

BACKUP()
{
    # Get backup path from main.sh
    backup_path=$1

    # Remove this line when implemented
    echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
    # Get restore path from main.sh
    restore_path=$1

    # Remove this line when implemented
    echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
    POSTINSTALL
    ;;
    PREUPDATE)
    PREUPDATE
    ;;
    POSTUPDATE)
    POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
    echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
