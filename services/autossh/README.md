Descriptif du service
=====================

Ce service permet d'établire une connexion ssh permanante vers un serveurs ssh. Cette connexion est réversible ce qui implique qu'il est possible d'atteindre le serveur qui a établit la connexion.

Info particulière déployement
-----------------------------

### Configuration de la machine distante

Ajouter la clef publique ssh du user "autossh" (dans `/home/autossh/.ssh/id_rsa.pub`) de la vm autossh dans la machine cible afin qu'elle puisse se connecter.

Modifier le `/etc/ssh/sshd_config` et ajouter/modifier la ligne suivante :

```
GatewayPorts clientspecified
```

Ensuite pour que la connexion s'établisse il sera nécessaire de valider la clef distante pour le client ssh. Donc se connecter en ssh au serveur distant et valider la clef via la commande suivante :

```
su - autossh
ssh USER@HOST -p PORT
```
Ensuite il faut redémarrer le service :
```
systemctl restart autossh_NOM
```


Cas de test après dépoyement
----------------------------

- Vérifier que au démarrage de la machine la connexion s'établisse
- Vérifier que en cas de coupure la connexion se rétablise dès que c'est possible.
- Tester une connexion inverse, donc accéder à la machine contenant le reverse ssh via la machine distante.

TODO
----

Voir Fichier CONTRIBUTING
