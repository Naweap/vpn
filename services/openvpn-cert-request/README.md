Descriptif du service
=====================

Service permettant de gérer les certificats openvpn facilement.

Utilisation du daemon de génération automatique (cert_manager)
--------------------------------------------------------------

Toute cette partie est codée en Rust afin d'offrir une sécurité optimum. Lors de l’exécution des scripts d'installation le code sera compilé et copié aux bons emplacements.

Ce daemon est démarré lors du lancement de la VM. Ce deamon permettra par l’intermédiaire de socket Unix de générer des certificat utilisateur.

Pour générer les certificat utilisateur nous avons l'architecture suivante :

- Un daemon qui écoute sur un socket Unix dans `/home/coin_request/daemon.socket`. Lors de son démarrage il ne connais PAS le mot de passe pour déchiffrer la clef de la CA. Il est donc nécessaire en premier nécessaire d'enregistrer le mot de passe auprès du daemon. Ce daemon stockera uniquement en rame le mot de passe. Lors qu'il a enregistré le mot de passe il sera capable de générer des certificat. Actuellement il utilise easyRSA afin de générer les certificats. Les paire de clef (certificat + clef privée) pour le client seront après génération et récupération du contenu supprimé. Ces clef seront ensuite envoyée au client par l'intermédiaire de coin.
- Un client nommé "client_register". Ce client permet uniquement à l'administrateur d’enregistrer dans le daemon le mot de passe de la CA. Cette opération devra être exectuée APRES CHAQUE DÉMARRAGE de la VM. Pour enregistrer le mot de passe il suffit d'exectuer l’exécutable `/root/client_register` et de saisir le mot de passe lorsque le programme le demande.
- Un client nommé "client_request". Il s'agit du programme utilisé par coin afin d'obtenir les certificat. Il suffit de l’exécuter avec comme argument le nom de l'utilisateur pour lequel on désire un certificat pour obtenir en STDOUT un contenu JSON avec toutes les clefs générée.

### Interraction avec Coin

Coin se connectera via ssh avec l'utilisateur `coin_request`. Ensuite l’exécution de `client_request` permettra à coin d'obtenir les clefs. Afin d'améliorer la sécurité et limiter les possibilité d’exécution le shell du user `coin_request` sera directement `coin_request`. Cela implique donc que coin se connectera juste en ssh avec comme argument l'utilisateur afin obtenir directement le certificat.

Info particulière déploiement
-----------------------------

Il est nécessaire d'ajouter dans `/home/coin_request/home/coin_request/.ssh/authorized_keys` la clef ssh du client coin afin qu'il soie capable d'obtenir le certificat.

D'autre part il est nécessaire d'ajouter le certificat de la CA du server et le fichier ta.key dans le répertoir `/root/openvpnCA/server_info/`.

Cas de test après déploiement
-----------------------------

- Tester l'enregistrement du mot de passe de la CA. Avec la commande `/root/client_register`.
- Tester la génération de certificats automatique. Pour cela on peut par exemple exécuter la commande suivante `/home/coin_request/client_request mytest`. Cela devrait retourner un contenu JSON avec toutes les clefs nécessaires.


TODO
----

- Tests
