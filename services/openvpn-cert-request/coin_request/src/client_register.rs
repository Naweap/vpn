#[macro_use]
extern crate json;
extern crate rpassword;

use std::io::prelude::*;
use std::io::{BufReader, Write};
use std::os::unix::net::{UnixStream};

const SOCKET_PATH: &'static str = "/home/coin_request/daemon.socket";

fn main() {
    println!("Please register the CA password");
    let password = rpassword::read_password().expect("Cant' read input");

    // Establish connexion
    let mut socket = match UnixStream::connect(SOCKET_PATH.to_owned()) {
        Ok(sock) => sock,
        Err(e) => {
            println!("Couldn't connect: {:?}", e);
            return
        }
    };
    // Send passord
    socket.write_all(object!{
        "request_type" => "set_password",
        "password" => password,
    }.dump().as_bytes()).expect("Can't send password to daemon");
    socket.write_all("\n".as_bytes()).expect("Can't send password to daemon");

    // Read if the daemon is ok
    let mut result = String::new();
    let mut buf = BufReader::new(socket);
    buf.read_line(&mut result).expect("Can't read answer from daemon");
    let result = json::parse(&result).expect("Can't decode answer");
    if result["op_success"] == true {
        println!("Register success");
    } else {
        println!("Register failled");
    }
//     socket.shutdown();
}

