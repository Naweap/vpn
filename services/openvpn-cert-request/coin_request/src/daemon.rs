/*
[package]
name = "build_client_cert"
version = "0.1.1"
authors = ["Josué Tille <josue@tille.ch>"]

[dependencies]
json = "0.11.13"
regex = "1.1.6"
rexpect = "0.3.0"
users = "0.9.1"
libc = "0.2.58"
rpassword = "3.0.2"
sendmail = "2.0.0"

[[bin]]
name = "deamon"
path = "src/daemon.rs"

[[bin]]
name = "client_request"
path = "src/client_request.rs"

[[bin]]
name = "client_register"
path = "src/client_register.rs"
*/
///////////////////////////////////////////////////////////////////////////////////////////

// How to build :
// - Install cargo if not done by : sudo apt install cargo
// - Make project : cargo init
// - Copy the toml file (at the to of this file) in : Cargo.toml
// - Add this file in : src/main.rs
// - Build debug version :  cargo build
// - Build final version : cargo build --release
// - You will find the binary in : target/debug/build_client_cert and target/release/build_client_cert

///////////////////////////////////////////////////////////////////////////////////////////

#[macro_use]
extern crate json;
extern crate regex;
extern crate rexpect;
extern crate users;
extern crate libc;
extern crate sendmail;

use std::{
    str,
    net::Shutdown,
    env,
    fs,
    fs::{File, OpenOptions},
    ffi::CString,
    io::{
        prelude::*,
        {Write, BufReader}
    },
    process::{Command, Stdio},
    os::unix::{
        process::CommandExt,
        net::{UnixStream, UnixListener},
    },
    sync::{
        Arc,
        atomic::{AtomicBool, Ordering}
    },
    time::Duration,
    thread,
};

use regex::Regex;
use rexpect::errors::*;

use users::get_user_by_name;
use libc::{chown,chmod};

use sendmail::email;

const ROOT_CA_PATH:  &'static str  = "/root/openvpnCA";
const EASYRSA_SCRIPT: &'static str = "/root/openvpnCA/easyrsa";
const SOCKET_PATH: &'static str    = "/home/coin_request/daemon.socket";
const SOCKET_USER_OWNER: &'static str = "coin_request";
const TEST_PWD_USER: &'static str  = "test_pwd_user";
const SOCKET_GID: u32              = 0;
const EXPECT_SCRIPT_UID:    u32    = 0;
const CERT_SIZE:            usize  = 4000;
const TA_SIZE:              usize  = 1000;
const WAIT_SEND_EMAIL_TIME: u64    = 5*60; // 5 min
const MAIL_REQUEST_PWD_CONTENT: &str =
"Hello,\n
\n
It look like that you have restarted the cert-request deamon but you haven't registred the password. You should probably do this !!!\n
\n
See you soon :-)\n";
const MAIL_REQUEST_PWD_SRC_ADDR: &'static str = "root@__VPN_CERT_REQUEST_FQDN__";
const MAIL_REQUEST_PWD_DST_ADDR: &'static str = "__ADMIN_EMAIL__";
const MAIL_REQUEST_PWD_SUBJECT: &'static str = "[Infra][VPN Certs] Cert request manager deamon need a password";

struct DaemonConfig {
    debug_mode : bool,
    manual_password : bool,
    password_is_defined : Arc<AtomicBool>,
    password_pwd : String
}

impl DaemonConfig {
    fn set_password(&mut self, new_password: String) {
        self.password_is_defined.store(true, Ordering::Relaxed);
        self.password_pwd = new_password.to_owned();
    }
    fn clean_password(&mut self) {
        self.password_is_defined.store(false, Ordering::Relaxed);
        self.password_pwd = "".to_owned();
    }
}

fn main() {
    // Remove old socket if exit
    match fs::remove_file(SOCKET_PATH.to_owned()) {
        Ok(_) => {},
        Err(_) => {}
    }

    let mut debug_mode = false;
    let mut manual_password = false;

    // Enable debug if set
    for argument in env::args() {
        match argument.as_str() {
            "debug" => {
                debug_mode = true;
            }
            "man_pwd" => {
                manual_password = true;
            }
            "help" => {
                println!("Option :");
                println!("debug : debug mode");
                println!("man_pwd : give the possiblity to see all interaction with easyrsa");
                return;
            }
            _ => {}
        }
    }

    // Create ca_password struct to be able to store the password
    let mut daemon_config = DaemonConfig {
        debug_mode : debug_mode,
        manual_password : manual_password,
        password_is_defined : Arc::new(AtomicBool::new(false)),
        password_pwd : String::new()
    };

    // Init socket
    let listener = match UnixListener::bind(SOCKET_PATH.to_owned()) {
        Ok(sock) => sock,
        Err(e) => {
            println!("Couldn't create socket at path {} : {:?}",SOCKET_PATH.to_owned() , e);
            return;
        }
    };
    // Define permission and owner for socket
    let socket_path = CString::new(SOCKET_PATH).expect(&("Can't create a string for ".to_owned() + &SOCKET_PATH.to_owned()));
    match get_user_by_name(SOCKET_USER_OWNER) {
        Some(user) => {
            unsafe {
                if chown(socket_path.as_ptr(), user.uid(), SOCKET_GID) != 0 {
                    println!("Couldn't change owner for socket at path {}",SOCKET_PATH.to_owned());
                }
                if chmod(socket_path.as_ptr(), 0o600) != 0 {
                    println!("Couldn't change permission for socket at path {}",SOCKET_PATH.to_owned());
                }
            }
        },
        None => {
            println!("Couldn't get user uid for {}", SOCKET_USER_OWNER);
        }
    }

    let password_defined = daemon_config.password_is_defined.clone();
    thread::spawn(move || send_mail_if_no_password_registred(password_defined));

    // Main loop to get all request
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                handle_client(stream, &mut daemon_config);
            }
            Err(err) => {
                println!("Can't get incomming connexion : {}", err);
            }
        }
    }
}

fn handle_client(mut stream: UnixStream, daemon_config : &mut DaemonConfig) {
    // Create copy of main buffer
    let mut buf = BufReader::new(match stream.try_clone() {
        Ok(b) => b,
        Err(e) => {
            operation_failled(stream, daemon_config, "Can't create a buffer. Erreur : ".to_owned() + &e.to_string());
            return;
        }
    });
    let stream2 = match stream.try_clone() {
        Ok(b) => b,
        Err(e) => {
            operation_failled(stream, daemon_config, "Can't create a buffer. Erreur : ".to_owned() + &e.to_string());
            return;
        }
    };
    // Get request
    let mut request = String::new();
    match buf.read_line(&mut request) {
        Ok(_) => {},
        Err(e) => {
            operation_failled(stream, daemon_config, "Can't read request from client. Erreur : ".to_owned() + &e.to_string());
            return
        }
    }
    let request = match json::parse(&request) {
        Ok(r) => r,
        Err(e) => {
            operation_failled(stream, daemon_config, "Can't decode request. Erreur : ".to_owned() + &e.to_string());
            return
        }
    };
    if (! daemon_config.password_is_defined.load(Ordering::Relaxed)) && (request["request_type"] == "set_password") {
        // Save password
        if request["password"].to_string().len() > 4 {
            daemon_config.set_password(request["password"].to_string());

            // Clean CA before to try to create cert
            match clean_user_in_ca(TEST_PWD_USER.to_owned()) {
                Ok(_) => {},
                Err(_) => {
                    operation_failled(stream, daemon_config, "Can't clean CA database".to_owned());
                    return;
                }
            }

            match call_easy_rsa(TEST_PWD_USER.to_owned(), daemon_config) {
                Ok(_) => {},
                Err(e) => {
                    daemon_config.clean_password();
                    operation_failled(stream, daemon_config, "Subscript failed. Probably bad password. Error : ".to_owned() + &e.to_string());
                    return;
                }
            }
        } else {
            operation_failled(stream, daemon_config, "Password too shord".to_owned());
            return;
        }
        match stream.write_all(object!{
            "op_success" => true,
        }.dump().as_bytes()) {
            Ok(_) => {},
            Err(e) => {
                operation_failled(stream, daemon_config, "Cant't write answer to client. Erreur : ".to_owned() + &e.to_string());
                return;
            }
        };
        match stream.write_all("\n".as_bytes()) {
            Ok(_) => {},
            Err(e) => {
                operation_failled(stream, daemon_config, "Cant't write answer to client. Erreur : ".to_owned() + &e.to_string());
                return;
            }
        };
    } else if (daemon_config.password_is_defined.load(Ordering::Relaxed)) && (request["request_type"] == "get_cert") {
        // Filter username
        let user_name = request["user_name"].to_string();
        if ! Regex::new(r"^[\w\n\-]+$").unwrap().is_match(&user_name) {
            operation_failled(stream, daemon_config, "username not valid".to_owned());
            return;
        }

        // Remove userName in CA database
        match clean_user_in_ca(user_name.to_owned()) {
            Ok(_) => {},
            Err(_) => {
                operation_failled(stream, daemon_config, "Can't clean CA database".to_owned());
                return;
            }
        }
        // Cert file declaration path
        let cert_file_path = [
            "/pki/issued/".to_owned() + &user_name + ".crt",
            "/pki/private/".to_owned() + &user_name + ".key",
            "/server_info/ca.crt".to_owned(),
            "/server_info/ta.key".to_owned(),
        ];
        // Clean old files, we can find some file when the last generation fail
        for p in [
            cert_file_path[0].to_owned(),
            cert_file_path[1].to_owned(),
            "/pki/reqs/".to_owned() + &user_name + ".req",
        ].iter() {
            let file_path = ROOT_CA_PATH.to_owned() + &p;
            match fs::remove_file(file_path.to_owned()) {
                Ok(_) => {},
                Err(_) => {} // Ignore error, normally theses file shouldn't exist
            }
        }
        // Launch the sub command
        match call_easy_rsa(user_name.to_owned(), daemon_config) {
            Ok(_) => {},
            Err(e) => {
                operation_failled(stream, daemon_config, "Subscript failled. Error : ".to_owned() + &e.to_string());
                return;
            }
        }
        // Read all cert file
        let mut result = [
            String::with_capacity(CERT_SIZE),
            String::with_capacity(CERT_SIZE),
            String::with_capacity(CERT_SIZE),
            String::with_capacity(TA_SIZE),
        ];
        for i in 0 .. cert_file_path.len() {
            let file_path = ROOT_CA_PATH.to_owned() + &cert_file_path[i];
            match get_file_cnt(file_path.to_owned(), &mut result[i]) {
                Ok(_) => {},
                Err(_) => {
                    operation_failled(stream, daemon_config, "Can't read file : ".to_owned() + &file_path);
                    return;
                }
            }
        }
        // Clean all cert failes
        for p in [
            cert_file_path[0].to_owned(),
            cert_file_path[1].to_owned(),
            "/pki/reqs/".to_owned() + &user_name + ".req",
        ].iter() {
            let file_path = ROOT_CA_PATH.to_owned() + &p;
            match fs::remove_file(file_path.to_owned()) {
                Ok(_) => {},
                Err(_) => {
                    operation_failled(stream, daemon_config, "Can't remove file : ".to_owned() + &file_path);
                    return;
                }
            }
        }
        // Show result as json
        let r = object!{
            "build_passed" => true,
            "crt" => result[0].to_owned(),
            "key" => result[1].to_owned(),
            "ca" => result[2].to_owned(),
            "ta" => result[3].to_owned(),
        };
        match stream.write_all(r.dump().as_bytes()) {
            Ok(_) => {},
            Err(e) => {
                operation_failled(stream, daemon_config, "Cant't write answer to client. Erreur : ".to_owned() + &e.to_string());
                return;
            }
        };
        match stream.write_all("\n".as_bytes()) {
            Ok(_) => {},
            Err(e) => {
                operation_failled(stream, daemon_config, "Cant't write answer to client. Erreur : ".to_owned() + &e.to_string());
                return;
            }
        };
    } else {
        operation_failled(stream, daemon_config, "Deamon not ready for this action".to_owned());
    }
    stream2.shutdown(Shutdown::Both).expect("shutdown stream failed");
}

fn call_easy_rsa(user_name : String, daemon_config : &DaemonConfig) -> Result<()> {
    let mut c = Command::new(EASYRSA_SCRIPT.to_owned());
    c.args(&["build-client-full", &user_name, "nopass"]);
    c.current_dir(&ROOT_CA_PATH);
    c.uid(EXPECT_SCRIPT_UID);
    if daemon_config.manual_password {
        c.stdout(Stdio::inherit());
        c.stdin(Stdio::inherit());
        c.stderr(Stdio::inherit());
        c.status().expect("Failed to execute command");
    } else {
        let mut p = rexpect::session::spawn_command(c, Some(8000))?;
        p.exp_regex(&("Enter pass phrase for ".to_owned() + ROOT_CA_PATH + "/pki/private/ca.key:"))?;
        p.send_line(&daemon_config.password_pwd)?;
        let result = p.exp_eof()?;
        if ! (result.contains(&"Data Base Updated") && result.contains(&"Write out database with 1 new entries")) {
            return Err(Error::from_kind(ErrorKind::Msg("Build process didn't generated a new certificate, output : ".to_owned() + &result)));
        }
        match p.process.wait().expect("Can't get result status of child process") {
            rexpect::process::wait::WaitStatus::Exited(_pid, status) => {
                if status != 0 {
                    return Err(Error::from_kind(ErrorKind::Msg("Build process failled with error :".to_owned() + &status.to_string())));
                }
            }
            _ => { return Err(Error::from_kind(ErrorKind::Msg("Unknown state of process".to_string())));}
        }
    }
    Ok(())
}

fn get_file_cnt(path : String, result : &mut String) -> std::io::Result<()> {
    let mut file = File::open(path)?;
    file.read_to_string(result)?;
    Ok(())
}

fn clean_user_in_ca(user_name : String) -> std::io::Result<()> {
    let file_path = ROOT_CA_PATH.to_owned() + "/pki/index.txt";
    let mut buffer_out = String::new();
    let file = File::open(&file_path)?;
    let file = BufReader::new(file);
    for line in file.lines() {
        let l = line.expect(&("Can't read line in file :".to_owned() + &file_path.to_owned()));
        if ! l.contains(&user_name) {
            buffer_out += &(l.to_owned() + "\n");
        }
    }
    let mut file = OpenOptions::new()
        .write(true)
        .open(&file_path)?;
    file.set_len(buffer_out.len() as u64)?;
    file.write_all(buffer_out.as_bytes())?;

    // Clean temporary directory if there still somes files
    for p in ["/pki/issued", "/pki/private", "/pki/reqs"].iter() {
        let dir_path = ROOT_CA_PATH.to_owned() + &p;
        for entry in fs::read_dir(dir_path.to_owned())? {
            let file = entry?;
            let file_name = file.file_name().into_string().expect("Can't extract filename");
            if file_name.starts_with(&(user_name.to_owned() + ".")) {
                fs::remove_file(file.path())?;
            }
        }
    }
    Ok(())
}

fn operation_failled(mut stream: UnixStream, daemon_config : &DaemonConfig, error : String) {
    println!("{}", error);
    let r;
    if daemon_config.debug_mode {
        r = object!{
            "op_success" => false,
            "build_passed" => false,
            "error" => error,
        };
    } else {
        r = object!{
            "op_success" => false,
            "build_passed" => false,
        };
    }
    match stream.write_all(r.dump().as_bytes()) {
        Ok(_) => {},
        Err(e) => {
            println!("Cant't write answer to client. Error : {}", e);
            stream.shutdown(Shutdown::Both).expect("shutdown stream failed");
            return;
        }
    };
    match stream.write_all("\n".as_bytes()) {
        Ok(_) => {},
        Err(e) => {
            println!("Cant't write answer to client. error : {}", e);
            stream.shutdown(Shutdown::Both).expect("shutdown stream failed");
            return;
        }
    };
    stream.shutdown(Shutdown::Both).expect("shutdown stream failed");
}

fn send_mail_if_no_password_registred(password_is_defined: Arc<AtomicBool>) {
    thread::sleep(Duration::from_secs(WAIT_SEND_EMAIL_TIME));
    if ! password_is_defined.load(Ordering::Relaxed) {
        match email::send(MAIL_REQUEST_PWD_SRC_ADDR,
                        &[MAIL_REQUEST_PWD_DST_ADDR],
                        MAIL_REQUEST_PWD_SUBJECT,
                        MAIL_REQUEST_PWD_CONTENT) {
            Ok(_) => {},
            Err(e) => {
                println!("Can't send email to request password. Error : {}", e);
            }
        };
    }
}
