#!/bin/bash

##################################################
# Service - openvpn certification
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data


coin_request_root="/home/coin_request"
coin_request_home="$coin_request_root/home/coin_request"
dependance="acl expect build-essential"

####################
# Shared functions
####################

build_and_install_coin_request() {
    [ -e /root/.cargo/bin/rustup ] || curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    (
        source /root/.cargo/env
        rustup update
        pushd tmp/openvpn-cert-request/coin_request
        cargo build --release
        cp target/release/client_register /root/client_register
        cp target/release/client_request $coin_request_root/client_request
        cp target/release/daemon /usr/local/bin/cert_manager
        popd
    )
    chmod 700 /usr/local/bin/cert_manager
    chown root:root /usr/local/bin/cert_manager
    chmod 700 /root/client_register
    chown root:root /root/client_register
    chown root:root -R $coin_request_root
    chown coin_request:root -R $coin_request_home
    chmod 700 $coin_request_root/client_request
    chown coin_request:root $coin_request_root/client_request
}

create_small_chroot() {
    # Use ldd what is the real dependance
    # # ldd client_register 
    # linux-vdso.so.1 (0x00007fff581ab000)
    # libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f608eebf000)
    # librt.so.1 => /lib/x86_64-linux-gnu/librt.so.1 (0x00007f608ecb7000)
    # libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f608ea9a000)
    # libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f608e883000)
    # libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f608e4e4000)
    # /lib64/ld-linux-x86-64.so.2 (0x00007f608f0c3000)
    # libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f608e1e0000)

    mkdir -p $coin_request_root/lib/x86_64-linux-gnu
    mkdir -p $coin_request_root/lib64
    mkdir -p $coin_request_home/.ssh
    cp /lib/x86_64-linux-gnu/libdl.so.2      $coin_request_root/lib/x86_64-linux-gnu/
    cp /lib/x86_64-linux-gnu/librt.so.1      $coin_request_root/lib/x86_64-linux-gnu/
    cp /lib/x86_64-linux-gnu/libpthread.so.0 $coin_request_root/lib/x86_64-linux-gnu/
    cp /lib/x86_64-linux-gnu/libgcc_s.so.1   $coin_request_root/lib/x86_64-linux-gnu/
    cp /lib/x86_64-linux-gnu/libc.so.6       $coin_request_root/lib/x86_64-linux-gnu/
    cp /lib64/ld-linux-x86-64.so.2           $coin_request_root/lib64/
    cp /lib/x86_64-linux-gnu/libm.so.6       $coin_request_root/lib/x86_64-linux-gnu/
}

####################
# Install
####################

PREINSTALL()
{
    apt-get install $dependance -y

    adduser --disabled-password --system \
        --shell /client_request \
        --home $coin_request_home \
        coin_request
}

POSTINSTALL() {
    systemctl daemon-reload
    systemctl enable cert_manager

    create_small_chroot
    build_and_install_coin_request
    systemctl start cert_manager
}

####################
# Update
####################

PREUPDATE()
{
    apt-get install $dependance -y
}

POSTUPDATE()
{
    systemctl daemon-reload

    create_small_chroot
    systemctl stop cert_manager
    build_and_install_coin_request
    systemctl start cert_manager
}


####################
# Backup / Restore
####################

BACKUP()
{
    # Get backup path from main.sh
    backup_path=$1

    # Remove this line when implemented
    echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
    # Get restore path from main.sh
    restore_path=$1

    # Remove this line when implemented
    echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
    POSTINSTALL
    ;;
    PREUPDATE)
    PREUPDATE
    ;;
    POSTUPDATE)
    POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
    echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
