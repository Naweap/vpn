#!/bin/bash

##################################################
# Service - ldap
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared functions
####################


####################
# Install
####################

PREINSTALL()
{
    echo "Donnez un password pour LDAP"
    read LDAP_PASSWORD

	echo "slapd slapd/no_configuration boolean no" | debconf-set-selections
	echo "slapd slapd/domain string $INFRA_DOMAIN" | debconf-set-selections
	echo "slapd shared/organization string $ASSOS_NAME" | debconf-set-selections
	echo "slapd slapd/password1 password $LDAP_PASSWORD" | debconf-set-selections
	echo "slapd slapd/password2 password $LDAP_PASSWORD" | debconf-set-selections
	echo "slapd slapd/purge_database boolean no" | debconf-set-selections
	echo "slapd slapd/backend select MDB" | debconf-set-selections

	apt-get install -y slapd ldapscripts
}

POSTINSTALL()
{
	# if there are a problem use '-c' option
	
	echo "ldapadd -x -c -w $LDAP_PASSWORD -D cn=admin,dc=infra,dc=swissneutral,dc=net  -H ldap://localhost -f services/ldap/data/database.ldif"

	ldapadd -x -c -w $LDAP_PASSWORD -D cn=admin,dc=infra,dc=swissneutral,dc=net  -H ldap://localhost -f services/ldap/data/database.ldif
	
	#ldapadd -Y EXTERNAL -H ldapi:/// -f config.ldif
}


####################
# Update
####################

PREUPDATE()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

POSTUPDATE()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
