Contributing
===================================

Pour les motivés à taper du code pour l'association, voici la liste des travaux en cours. Vous y trouverez tous les projets sur lesquels nous travaillons actuellement, leur responsable ainsi que les membres participants. N'hésitez pas à contacter les membres responsables pour participer ou pour plus d'informations.

Merci pour votre contribution !

TODO Services
=============

autossh
-------

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : Samuel Aymon
- Description           : Voir dans `services/ssh/README.md`
- Tâches à effectuer    :
    - Review

bind
----

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : Florian
- Description           : Voir dans `services/bind/README.md`
- Tâches à effectuer    :
    - TLSA
    - Cron rotation clef
    - Review
    - Tests selons cas de tests

bind_slave
----------

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : Florian
- Description           : Voir dans `services/bind_slave/README.md`
- Tâches à effectuer    :
    - Review
    - Tests selons cas de tests

fail2ban
--------

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : 
- Description           : Voir dans `services/fail2ban/README.md`
- Tâches à effectuer    :
    - Vérifier mise à jours du port ssh custom
    - Ajouter blacklist manuel IPs
    - Review
    - Tests selons cas de tests

firewall-router
---------------

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : 
- Description           : Voir dans `services/firewall-router/README.md`
- Tâches à effectuer    :
    - Tests selons cas de tests

firewall
--------

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : Samuel Aymon
- Description           : Voir dans `services/firewall/README.md`
- Tâches à effectuer    :
    - Tests selons cas de tests

frrouting
---------

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : 
- Description           : Voir dans `services/frrouting/README.md`
- Tâches à effectuer    :
    - Résoudre routage IPv6
    - Tests selons cas de tests

hwraid
------

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : 
- Description           : Voir dans `services/hwraid/README.md`
- Tâches à effectuer    : Rien

locales
-------

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : 
- Description           : Voir dans `services/locales/README.md`
- Tâches à effectuer    : Rien

monitoring
----------

- Responsable           : Non définit
- Membres participants  :
- Enciens participants  :
- Description           : Monitoring de l'infra (notamment les mail sortant)
- Tâches à effectuer    :
    - Choix d'un produit (Zabbix ??)
    - Création scripts gestions mails sortant
    - Création de sondes de trafic des clients (pour coin)
    - Création d'autres sondes
    - Certificats TLS
    - Clef TLSA
    - Intégration dans l'interface de coin du trafic des clients VPN

openvpn-cert
------------

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : 
- Description           : Voir dans `services/openvpn-cert/README.md`
- Tâches à effectuer    :
    - Review
    - Tests selons cas de tests

openvpn
-------

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  :
- Description           : Voir dans `services/openvpn/README.md`
- Tâches à effectuer    :
    - Review
    - Tests selons cas de tests

resolver
--------

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  :
- Description           : Voir dans `services/resolver/README.md`
- Tâches à effectuer    :
    - Review
    - Tests selons cas de tests

security_update
---------------

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : 
- Description           : Voir dans `services/security_update/README.md`
- Tâches à effectuer    :
    - Tests selons cas de tests

ssh
---

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : Samuel Aymon
- Description           : Voir dans `services/ssh/README.md`
- Tâches à effectuer    :
    - Tests selons cas de tests
    - Voir solution pour augmenter la sécurité

time
----

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : 
- Description           : Voir dans `services/time/README.md`
- Tâches à effectuer    :
    - Tests selons cas de tests

tmux
----

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : 
- Description           : Voir dans `services/time/README.md`
- Tâches à effectuer    :
    - Créer le service
    - Tests selons cas de tests

yunohost
--------

- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : 
- Description           : Voir dans `services/yunohost/README.md`
- Tâches à effectuer    :
    - Review
    - Tests selons cas de tests

TODO Global
===========

Normalisation des scripts
-------------------------
- Responsable           : Josué Tille
- Membres participants  :
- Enciens participants  : Samuel Aymon
- Description           :
    - Normalisation des scripts qui seront écrits dans le git.
- Tâches à effectuer :
    - Review des scripts actuels
    - Backups
