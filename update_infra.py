#!/usr/bin/python3

# Tunnel number to use. Should be between 1-9
n = 1

# BEGIN CONFIG EXAMPLE

# Example
container_list_example = [
    {'addr': '1.2.3.4', 'port': '12', 'username': 'root', 'critical': False, 'tunnel_name': None },
    {'addr': '1.2.3.4', 'port': '12', 'username': 'root', 'critical': False, 'tunnel_name': 'prod' },
    {'addr': '1.2.3.4', 'port': '12', 'username': 'root', 'critical': False, 'tunnel_name': 'rescure', 'upgrade_cmd': 'unattended-upgrade' },
]

tunnel_config_example = [
    {'name': 'example',  'cmd': 'ssh -p 1234 root@1.2.3.4 ...'},
]

# END CONFIG EXAMPLE

###################################################################################################

# BEGIN CONFIG

container_list_dev = [
    {'name': 'Main NAT',       'addr': '109.205.169.146', 'port': '10110', 'username': 'root', 'critical': True, 'tunnel_name': None },
    {'name': 'Hyperviser',     'addr': '109.205.169.146', 'port': '22122', 'username': 'root', 'critical': True, 'tunnel_name': None, 'upgrade_cmd': 'unattended-upgrade' },
]

container_list_dev_test = [
    # ... available in the internal wiki
]

container_list_prod = [
    # ... available in the internal wiki
]

tunnel_config = [
    {'name': 'dev',  'cmd': '''ssh \
            -o PermitLocalCommand=yes \
            -i /home/josue/.ssh/id_ed25519 \
            -o LocalCommand="ip address add 10.8.245.$((4*%s-2))/30 dev tun%s;
                             ip link set tun%s up;
                             ip route add 10.10.20.0/24 via 10.8.245.$((4*%s-3));
                             ip route add 10.10.30.0/24 via 10.8.245.$((4*%s-3))" \
            -o ServerAliveInterval=60 \
            -w %s:5 -C \
            root@185.250.56.18 -p 10110 \
            "ip address add 10.8.245.$((4*%s-3))/30 dev tun5; ip link set tun5 up; echo tun%s ready"''' % tuple([n]*8)}
]

# END CONFIG

###################################################################################################

from subprocess import Popen, PIPE
import time
import sys
from datetime import datetime
import shlex

# Constant
LOG_DIR_PATH = "/root/upgrade_script"
LOG_FILE_PATH = LOG_DIR_PATH + "/" + datetime.today().isoformat('_', 'seconds') + ".log"
UPDATE_CMD = "apt-get update"
UPGRADE_CMD = "apt-get upgrade -y && apt-get dist-upgrade"
POST_UPDATE_CMD = "apt-get autoclean && apt-get install -y debian-goodies"
CHECK_RESTART_CMD = "checkrestart"
CHECK_RESTART_NO_PROC_STR = "Found 0 processes using old versions of upgraded files"
RESTART_CMD = "reboot"
PING_CMD = "ping"
REBOOT_TIMEOUT = 60
SSH_OPTIONS = ['-o', 'BatchMode=yes']

###################################################################################################


def get_remote_cmd(ssh_cmd, remote_cmd):
    cmd = ssh_cmd[:]
    cmd.append("(mkdir -p " + LOG_DIR_PATH + " && " +
               remote_cmd + ") | tee -a " + LOG_FILE_PATH)
    return cmd


def reboot_ct(ssh_cmd, container):
    print("Restart container : '%s'" % container['name'])
    with Popen(get_remote_cmd(ssh_cmd, RESTART_CMD)) as proc:
        proc.wait()
    time.sleep(5)
    for _ in range(0, REBOOT_TIMEOUT):
        with Popen([PING_CMD, '-c1', container['addr']]) as proc:
            proc.wait()
            if proc.returncode == 0:
                break
        time.sleep(2)
    i = 1
    while i <= 10:
        time.sleep(i)
        with Popen(get_remote_cmd(ssh_cmd, "exit")) as proc:
            proc.wait()
            if proc.returncode == 0:
                break
        i += 1
    if i == 10:
        print("Can't access to container : '%s'" % container['name'])
    else:
        print("Container succesfully updated")


def reboot_services(ssh_cmd, container, check_restart_res):
    service_restart_cmd = ""
    for line in check_restart_res.splitlines():
        if line.startswith("service ") or line.startswith("systemctl "):
            if service_restart_cmd == "":
                service_restart_cmd += line
            else:
                service_restart_cmd += " && " + line

    if service_restart_cmd != "":
        with Popen(get_remote_cmd(ssh_cmd, service_restart_cmd)) as proc:
            proc.wait()
        if (proc.returncode != 0):
            print("ERROR connection to container : '%s'" % container['name'])
            return proc.returncode
    else:
        print("No service to restart found")
    return 0


def upgrade_ct(tunnel_name=None):
    for container in container_list:
        if container['tunnel_name'] != tunnel_name:
            continue

        print("\n" + "-"*30)
        print("Update container '%s' at IP %s" %
              (container['name'], container['addr']))
        print("-"*30 + "\n")

        ssh_cmd = ['ssh', *SSH_OPTIONS, '-p', container['port'],
                   container['username'] + "@" + container['addr']]

        next_cmd = (container['update_cmd'] if 'update_cmd' in container else UPDATE_CMD) \
            + " && " + (container['upgrade_cmd'] if 'upgrade_cmd' in container else UPGRADE_CMD) \
            + " && " + (container['post_upgrade_cmd']
                        if 'post_upgrade_cmd' in container else POST_UPDATE_CMD)

        with Popen(get_remote_cmd(ssh_cmd, next_cmd)) as proc:
            proc.wait()
        if (proc.returncode != 0):
            print("ERROR connection to container : '%s'" % container['name'])
            return proc.returncode

        while True:
            print("Check restart")
            with Popen(get_remote_cmd(ssh_cmd, CHECK_RESTART_CMD), stdout=PIPE, encoding="UTF-8") as proc:
                res, err = proc.communicate()
                print(str(res))
                proc.wait()
            if (proc.returncode != 0):
                print("ERROR connection to container : '%s'" %
                      container['name'])
                return proc.returncode

            if CHECK_RESTART_NO_PROC_STR not in res:
                if container['critical']:
                    print("A critical server need to restart some service, what do you want to do ?")
                    print("Checkrestart result")
                    print('-' * 30)
                    print(res)
                    print('-' * 30)
                    print("What do you want to do ?")
                    print("r\t\tTry restart services")
                    print("total_reboot\tReboot the machine")
                    print("n\t\tDo nothing")
                    answer = input()
                    if answer == 'r':
                        returncode = reboot_services(ssh_cmd, container, res)
                        if returncode != 0:
                            return returncode

                    elif answer == 'total_reboot':
                        reboot_ct(ssh_cmd, container)
                        break
                    else:
                        print("Do nothing continue")
                        break
                else:
                    reboot_ct(ssh_cmd, container)
                    break
            else:
                break
    return 0


# Chose the config to execute
container_list = container_list_dev
if len(sys.argv) == 2:
    if sys.argv[1] == 'dev':
        container_list = container_list_dev
    elif sys.argv[1] == 'test':
        container_list = container_list_dev_test
    elif sys.argv[1] == 'prod':
        container_list = container_list_prod
else:
    print("Usage:")
    print("./update_infra.py dev|test|prod")
    exit(1)

tunnel_needed = []
for container in container_list:
    if container['tunnel_name'] not in tunnel_needed:
        tunnel_needed.append(container['tunnel_name'])

for tunnel_name in tunnel_needed:
    if tunnel_name:
        for tun in tunnel_config:
            if tun['name'] == tunnel_name:
                tunnel = tun
                break
        print("Make tunnel : %s" % tunnel['name'])
        ssh_tunnel_prod_cmd = shlex.split('sudo ' + tunnel['cmd'])

        with Popen(ssh_tunnel_prod_cmd, stdout=PIPE) as proc:
            tun_result = proc.stdout.readline()
            if tun_result.decode() != 'tun%s ready\n' % n:
                pkill = Popen(['sudo', 'kill', str(proc.pid)])
                proc.terminate()
                print("Error")
                exit(1)

            res = upgrade_ct(tunnel['name'])
            print("Close tunnel")
            with Popen(shlex.split(r'''bash -c 'sudo kill -2 $(ps -ao pid,ppid | grep -P -o "\d+ +%s" | cut -d" " -f1)' ''' % proc.pid)) as kproc:
                kproc.wait()

            proc.wait()
            if proc.returncode != 0:
                print("Error")
                exit(1)

    else:
        res = upgrade_ct()

    if res != 0:
        print("Error")
        exit(1)
