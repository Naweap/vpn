#!/bin/bash

###################################################################
# Définitions des variables globales propres à la machine virtuelle
###################################################################

# Services supplémentaires à installer sur cette vm
LOCAL_SERVICES=""

SSH_TUNNEL_ALLOWED="point-to-point"

# Network interfaces
WAN_IFACE="enx000ec6e1e9b0"
LAN30_IFACE="eth0"
LAN31_IFACE="eth1"
