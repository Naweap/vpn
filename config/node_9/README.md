Fonctions de la machine physique :
---------------------------------

- Accès bas niveau au serveur pour l'admin à distance.

Info déployement
----------------

### Création RootFS et Configuration

Création du système de fichier de base sur la carte SD :

On partira du principe que la carte SD est monté dans `/media/sdcard`.

```
cd /media
debootstrap --verbose --foreign --arch armhf stretch sdca http://ftp.debian.org/debian
```

Ensuite on executera les commande suivante avec du matériel avec une architecture impérativement compatible.

Remonter la carte si ce n'est pas fait dans `/media/sdcard`.

```
chroot /media/sdcard
mount -t proc proc /proc
/debootstrap/debootstrap --second-stage
```

Adapter le fichier `/etc/resolv.conf` avec le contenu du git. On utilisera les serveurs DNS de FDN afin d'avoir un source de DNS indépendance de notre infra. Il est conseillé de verouiller ce fichier via la commande `chattr +i /etc/resolv.conf` afin qu'il ne soie pas modifié lord de requête DHCP. Il peut être déverouillé via la commande `chattr -i /etc/resolv.conf`.

Adapter aussi les fichier `/etc/host`, `/etc/hostname`, `/etc/apt/sources.list`, `/etc/fstab` (attention à adapter les chemin des partitions).

Ensuite on peut installer les paquet de base :

```
apt update
apt install udev openssh-server locales console-data bash-completion console-setup ntpdate ethtool hdparm dracut iwlist wireless-tools vlan
tasksel install standard
```

On peut ensuite définir un password root via la commande `passwd`.


Utilisation
-----------

- Pour des info générale de conception des tunnels SSH voir le README du service SSH.
- Pour obtenir la commande spécifique voir README_priv.md

Procédure pour protéger la carte SD
-----------------------------------

### Mise en place de Log2Ram

Pour éviter de d'avoir trop d'écritures inutiles dans /var/log on utilsera log2ram qui va stocker en RAM tous les log et les sychronisera prériodiquement sur la mémoire flash. On aura donc beaucoup moin d'écritures.

Doc : https://github.com/azlux/log2ram

### Montage de /tmp en ram

Voir fichier fstab dans "Node_config"

### Utilisation de zram

Cette solution n'est pas disponible sur debian stretch elle le sera uniquement à partir de debian buster.

Pour cela installer le paquet debian zram-tools.

### Autre

Voir aussi : https://sebsauvage.net/wiki/doku.php?id=linux-ssd
