Fonctions de la machine virtuelle :
---------------------------------

- Serveur Relais ssh pour accéder à l'infrastrcture. Il s'agit ici du serveur qui va être configuré à l'extérieur de notre infrastrcture. Ce serveur recevra les connexion des client (administrateurs et redirigera vers le serveur "autossh" de notre infra.


Utilisation
-----------

- Pour des info générale de conception des tunnels SSH voir le README du service SSH.
- Pour obtenir la commande spécifique voir README_priv.md