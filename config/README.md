Répertoire "config" :
------------------------

- Contient les dossiers de configuration des machines virtuelles et des nodes.
- Contient les variables globales.


Structure :
-----------

Dossier sys_all : Contient la configuration commune à toutes les machines virtuelles et physiques.

Dossiers vm_0 : Contient un exemple de configuration de machine virtuelle sans service autre que les services listés dans les configurations vm_all.

Dossier vm_1, vm_2, vm_NUM... : Contient la configuration de la machine virtuelle numérotée.

On utilisera la même structure pour les nodes

Fichiers de configurations :
----------------------------

Chaque répertoire de configuration de machine virtuelle contient deux fichiers.

config.sh : Contient les variables globales de tous les services installés sur cette machine à l'exception des variables sensibles.

private.sh : Contient les variables sensibles (port ssh, informations secretes). Ce fichier contient de base la liste des variables globales secretes avec des valeurs de test. Changez ces valeurs juste avant de lancer l'installation sur la machine virtuelle.



# Strucuture des VM :

Chaques VM aura une utilité définie. Voici la liste des vm avec leur utilité :

- vm_0  EXEMPLE - Copiez cette configuration dans un nouveau répertoire vm_NUM pour créer une nouvelle machine sans service supplémentaire.

- vm_200    Résolveur DNS
- vm_201    Résolveur DNS
- vm_202    bind authoritative master
- vm_203    bind authoritative slave 1
- vm_302    OpenVPN
- vm_303    Serveur Relay SSH
- vm_304    Yunohost
- vm_330    openvpn-CA Server
- vm_331    openvpn-CA Client
- vm_600    Relais SSH-VPN (hors infra prod)

Non utilisé actuellement :

- vm_2  WebAdmin LDAP

# Strucuture des Nodes :

- node_0  EXEMPLE - Copiez cette configuration dans un nouveau répertoire node_NUM pour créer une nouvelle machine.
- node_1    1er Serveur déployé
- node_2    2eme Serveur déployé (serveur de backup)
- node_9    Serveur Accès de secours
