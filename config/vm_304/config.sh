#!/bin/bash

###################################################################
# Définitions des variables globales propres à la machine virtuelle
###################################################################

# Services supplémentaires à installer sur cette vm
ROOT_SERVICES="locales ssh security_update tmux"
LOCAL_SERVICES="yunohost"

# Yunohost
YUNOHOST_DOMAIN="yunohost.$MAIN_DOMAIN"
