#!/bin/bash

###################################################################
# Définitions des variables globales propres à la machine virtuelle
###################################################################

# Services supplémentaires à installer sur cette vm
ROOT_SERVICES="locales ssh security_update fail2ban tmux mailforward"
LOCAL_SERVICES="firewall-router frrouting"
