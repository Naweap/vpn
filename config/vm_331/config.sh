#!/bin/bash

###################################################################
# Définitions des variables globales propres à la machine virtuelle
###################################################################

# Services supplémentaires à installer sur cette vm
LOCAL_SERVICES="openvpn-cert openvpn-cert-request"
