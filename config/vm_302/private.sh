#!/bin/bash

###########################################################################
# Définitions des variables globales privées propres à la machine virtuelle
###########################################################################

# Liste des administrateurs de cette machine
# ADMINS_LIST="user_1 user_2"

# ssh
SSH_PORT="22"

# Liste des ports à ouvrir
FIREWALL_UDP_PORT=""
FIREWALL_TCP_PORT=""

# Firewall
FIREWALL_IP4_CUSTOM_RULE="
    iptables -A FORWARD -i in_interface -o out_interface -j ACCEPT
"
FIREWALL_IP6_CUSTOM_RULE="
    ip6tables -A FORWARD -i in_interface -o out_interface -j ACCEPT
"
