Fonctions de la machine physique :
---------------------------------

- Hyperviseur - serveur de production



Info déployement
----------------

### Installation proxmox

Afin d'obtenir les mises à jours de proxmox il est nécessaire remplacer la ligne du fichier `/etc/apt/sources.list.d/pve-enterprise.list` suivante :
```
deb https://enterprise.proxmox.com/debian/pve buster pve-enterprise

```

Par la ligne :

```
deb http://download.proxmox.com/debian/pve buster pve-no-subscription
```
