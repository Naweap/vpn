Fonctions de la machine physique :
---------------------------------

- Backup hors site


Info déployement
----------------

### Installation proxmox

Afin d'obtenir les mises à jours de proxmox il est nécessaire remplacer la ligne du fichier `/etc/apt/sources.list.d/pve-enterprise.list` suivante :
```
deb https://enterprise.proxmox.com/debian/pve buster pve-enterprise

```

Par la ligne :

```
deb http://download.proxmox.com/debian/pve buster pve-no-subscription
```

### Configuration Backup

Installer PVE-zsync: `apt install pve-zsync`.

Afin de démarrer de démarrer l'hyperviseur il sera nécessaire d’exécuter le script suivant via le container de relais SSH:

```bash
#!/bin/bash

server_url='http://addr_IPMI'
username="admin"
password="the_password"

cookie=$(curl "$server_url/rpc/WEBSES/create.asp" -H "Origin: $server_url" -H 'DNT: 1' -H 'Connection: keep-alive' -H "Referer: $server_url/page/login.html" -H "Cookie: test=1; SessionCookie=LOGGED_OUT; Username=$admin" --data-raw "WEBVAR_USERNAME=$username&WEBVAR_PASSWORD=$password"| grep SESSION_COOKIE | cut -d\' -f4)

curl "$server_url/rpc/hostctl.asp" -H "Origin: $server_url" -H 'DNT: 1' -H 'Connection: keep-alive' -H "Referer: $server_url/page/server_power_control.html" -H "Cookie: test=1; SessionCookie=$cookie; Username=$username; lItem=3; lastNav=RMCNTRL_LEFTNAV; lastHiLit=STR_TOPNAV_REMOTE_CONTROL" --data-raw 'WEBVAR_POWER_CMD=1'
```


