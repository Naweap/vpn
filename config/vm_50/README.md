Fonctions de la machine virtuelle :
---------------------------------

- Relais d'accès au serveur de backup. Permet l'accès à l'hyperviseur Proxmox ainsi que à l'IPMI (interface gestion alimentation).
